

<?php require_once "./code.php";?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S2</title>
</head>
<body>

	<h2>Divisible of Five</h2>
	<?php modifiedForLoop(); ?>


<h2>Array Manipulation</h2>

<?php array_push($students, 'John Smith'); ?>
<pre><?php var_dump($students); ?></pre>
<p><?= count($students); ?></p>

<?php array_push($students, 'Jane Smith'); ?>
<pre><?php var_dump($students); ?></pre>
<p><?= count($students); ?></p>

<?php array_shift($students); ?>
<pre><?php var_dump($students); ?></pre>
<p><?= count($students); ?></p>

</body>
</html>



